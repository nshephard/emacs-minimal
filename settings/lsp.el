;;; LSP CONFIGURATION
;;; --------------------------------------
;;; https://gitlab.com/nathanfurnal/dotemacs/-/snippets/2060535
;;; https://ianyepan.github.io/posts/emacs-ide/
;; Provides workspaces with file browsing (tree file viewer)
;; and project management when coupled with `projectile`.
(use-package treemacs
	     :ensure t
	     :defer t
	     :config
	     (setq treemacs-no-png-images t
		   treemacs-width 24)
	     :bind ("C-c t" . treemacs))


;; Provide LSP-mode for python, it requires a language server.
;; I use jedi-language-server loaded by lsp-jedi below.
;; Know that you have to `M-x lsp-restart-workspace`
;; if you change the virtual environment in an open python buffer.
(use-package lsp-mode
	     :ensure t
	     :defer t
	     :config
	     (setq lsp-idle-delay 0.5
		   lsp-enable-symbol-highlighting t
		   lsp-pylsp-plugins-pylint-args ["--rcfile=/home/neil/dotfiles/python/.pylintrc"])
	     ;; (setq lsp-markdown-server-command remark-language-server)
	     ;; (lsp-register-custom-settings
	     ;;  ;; pyls enabled/disabled https://github.com/python-lsp/python-lsp-server/blob/develop/CONFIGURATION.md
	     ;;  '(;; Enabled
	     ;;    ("pylsp.plugins.jedi_completion.enabled" t t)
	     ;;    ("pylsp.plugins.jedi_completion.cache_for" '(pandas, numpy, matplotlib))
	     ;;    ("pylsp.plugins.jedi_hover.enabled" t t)
	     ;;    ("pylsp.plugins.jedi_references.enabled" t t)
	     ;;    ("pylsp.plugins.pyls_black.enabled" t t)
	     ;;    ("pylsp.plugins.pycodestyle.maxLineLength" nil 120)
	     ;;    ;; ("pylsp.plugins.pydocstyle.enabled" t t)
	     ;;    ;; ("pylsp.plugins.pydocstyle.convention" nil 'numpy)
	     ;;    ("pylsp.plugins.pylint.enabled" t t)
	     ;;    ;; Disabled (duplicated by flake8)
	     ;;    ("pylsp.plugins.pycodestyle.enabled" nil t)
	     ;;    ("pylsp.plugins.mccabe.enabled" nil t)
	     ;;    ("pylsp.plugins.pyflakes.enabled" nil t)))
	     :commands (lsp lsp-deferred)
	     :init (setq lsp-keymap-prefix "C-c l"
			 lsp-bash-highlight-parsing-errors t)
	     :hook ((python-mode . lsp)
		    (bash-mode . lsp)
		    (dockerfile-mode . lsp)
		    (groovy-mode . lsp)
		    (html-mode . lsp)
		    (latex-mode . lsp)
		    (markdown-mode . lsp)
		    (gfm-mode . lsp)
		    (org-mode . lsp)
		    (R-mode . lsp)
		    (ess-r-mode . lsp)
		    (sh-mode . lsp)
		    (terraform-mode . lsp)))

;; Provides visual help in the buffer
;; For example definitions on hover.
;; The `imenu` lets me browse definitions quickly.
;; https://github.com/emacs-lsp/lsp-ui
(use-package lsp-ui
	     :ensure t
	     :defer t
	     :config
	     (setq lsp-ui-doc-enable t
		   lsp-ui-doc-delay 1
		   lsp-ui-doc-header nil
		   sp-ui-doc-include-signature t
		   lsp-ui-doc-border (face-foreground 'default)
		   lsp-ui-doc-use-childframe t
		   lsp-ui-doc-position 'top
		   lsp-ui-doc-include-signature t
		   lsp-ui-doc-use-childframe t
		   lsp-ui-sideline-enable nil
		   lsp-ui-flycheck-enable t
		   lsp-ui-flycheck-list-position 'right
		   lsp-ui-flycheck-live-reporting t
		   lsp-ui-peek-enable t
		   lsp-ui-peek-list-width 60
		   lsp-ui-peek-peek-height 25
		   lsp-ui-sideline-enable t
		   lsp-ui-sideline-show-code-actions t
		   lsp-ui-sideline-show-hover t
		   lsp-ui-sideline-delay 3)
	     :hook (lsp-mode . lsp-ui-mode)
	     :bind (:map lsp-ui-mode-map
			 ("C-c i" . lsp-ui-imenu)))

;; LSP Treemacs
(use-package lsp-treemacs
	     :ensure t
	     :defer t
	     :config
	     (setq lsp-treemacs-sync-mode 1))

;; Integration with the debug server
(use-package dap-mode
	     :ensure t
	     :defer t
	     :after lsp-mode
	     :config
	     (dap-auto-configure-mode))


;; Required to hide the modeline
(use-package hide-mode-line
	     :ensure t
	     :defer t)

;; Python - Jedi
;; LSP Module : https://github.com/fredcamps/lsp-jedi
;; Server     : https://github.com/pappasam/jedi-language-server
(use-package lsp-jedi
	     :ensure t
	     :config
	     (with-eval-after-load "lsp-mode"
	       (add-to-list 'lsp-disabled-clients 'pyls)
	       (add-to-list 'lsp-enabled-clients 'jedi)))

;; LTex
;; LSP Module : https://github.com/emacs-languagetool/lsp-ltex
;; Server     : https://valentjn.github.io/ltex/
(use-package lsp-ltex
	     :ensure t
	     :hook (text-mode . (lambda ()
				  (require 'lsp-ltex)
				  (lsp))))  ; or lsp-deferred
(use-package lsp-latex
	     :ensure t
	     :hook (text-mode . (lambda ()
				  (require 'lsp-latex)
				  (lsp))))
;; Julia
;; LSP Module : https://github.com/non-Jedi/lsp-julia
;; Server     :
(use-package lsp-julia
	     :ensure t
	     :config
	     (setq lsp-julia-default-environment "~/.julia/environments/v1.5"))

;; Cleanup LSP sessions https://arjenwiersma.nl/posts/2022-11-07-cleaning-up-after-lsp/index.html
(defun nds/cleanup-lsp ()
  "Remove all the workspace folders from LSP"
  (interactive)
  (let ((folders (lsp-session-folders (lsp-session))))
    (while folders
      (lsp-workspace-folders-remove (car folders))
      (setq folders (cdr folders)))))
