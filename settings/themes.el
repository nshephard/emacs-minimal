;; THEME CONFIGURATION
;; --------------------------------------
;;
;; I've tried out lots, one day I might settle on one.
;; Modus Themes (Vivendi) https://protesilaos.com/modus-themes/
;; EF Themes https://protesilaos.com/emacs/ef-themes
;; Customisation : https://protesilaos.com/codelog/2023-01-01-modus-themes-4-0-0/
;; Old versions  : https://systemcrafters.net/emacs-from-scratch/the-modus-themes/
;;
;; Choose to render some code constructs in slanted text (italics).  The
;; default, shown below, is to not use italics, unless it is absolutely
;; necessary.
(use-package modus-themes
	     :ensure t                         ; omit this to use the built-in themes
	     :init
	     ;; Add all your customizations prior to loading the themes
	     (setq modus-themes-italic-constructs t
		   modus-themes-bold-constructs t
		   modus-themes-org-blocks '(tinted-background))
	     :config
             :bind
             ("<f12>" . modus-themes-toggle))

(modus-themes-select 'modus-vivendi) ;; OR modus-operandi

(use-package ef-themes
	     :ensure t
	     :init
             (setq ef-themes-disable-other-themes 'ef-themes-light-themes)
	     :config
	     :bind
	     ("<f10>" . ef-themes-select-dark)
             ("<f11>" . ef-themes-toggle))
;; (ef-themes-select 'ef-dark)
;; (ef-themes-select 'ef-duo-dark)
(ef-themes-select 'ef-bio)
;; (ef-themes-select 'ef-symbiosis)
;; (ef-themes-select 'ef-night)
;; (ef-themes-select 'ef-trio-dark)
;; (ef-themes-select 'ef-autumn)
;; (ef-themes-select 'ef-winter)
