;;; POLYMODE CONFIGURATION
;;; --------------------------------------
;;;
;;; GitHub : https://github.com/polymode/polymode
;;; Documentation : https://polymode.github.io/
;;; poly-markdown : https://github.com/polymode/poly-markdown
;;; poly-noweb : https://github.com/polymode/poly-noweb
;;; poly-org : https://github.com/polymode/poly-org
;;; poly-R : https://github.com/polymode/poly-R
;;; poly-rst : https://github.com/polymode/poly-rst
(use-package polymode
	     :ensure t
	     :defer t)
(use-package poly-markdown
	     :ensure t
	     :defer t)
(use-package poly-noweb
	     :ensure t
	     :defer t)
(use-package poly-org
	     :ensure t
	     :defer t)
(use-package poly-R
	     :ensure t
	     :defer t)
(use-package poly-rst
	     :ensure t
	     :defer t)
